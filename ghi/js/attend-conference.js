window.addEventListener('DOMContentLoaded', async () => {
    const conferenceTag = document.getElementById('conference');
    const loadingTag = document.getElementById('loading-conference-spinner')
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
  
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        conferenceTag.appendChild(option);
      }

      conferenceTag.classList.remove("d-none")
      loadingTag.classList.add("d-none")
    }

    const successTag = document.getElementById('success-message')
    const formTag = document.getElementById('create-attendee-form')
    formTag.addEventListener('submit', async event => {
      event.preventDefault()
      const formData = new FormData(formTag)
      const json = JSON.stringify(Object.fromEntries(formData))
      const data = JSON.parse(json)
      console.log(data)
      const conferenceURL = `http://localhost:8001${data.conference}attendees/`
      const attendeeInfo = {name : data.name, email: data.email}
      const attendeeJson = JSON.stringify(attendeeInfo)
      const fetchConfig = {
      method: "post",
      body: attendeeJson,
      headers: {
          'Content-Type': 'application/json',
      },
      }
      const response = await fetch(conferenceURL, fetchConfig)
      if (response.ok) {
      successTag.classList.remove('d-none')
      formTag.classList.add('d-none')
      }  
    })
  
  });