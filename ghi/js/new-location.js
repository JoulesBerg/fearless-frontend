window.addEventListener('DOMContentLoaded', async() => {

const states_url = "http://localhost:8000/api/states/"

try{
const stateResponse = await fetch(states_url) 

if (!stateResponse.ok){
    throw new Error('Response not ok')
}
else{
    const stateData = await stateResponse.json()

    const selectTag = document.getElementById('state')
    for (let state of stateData.states) {
      // Create an 'option' element
        const option = `<option value="${state.abbreviation}">${state.name}</option>`
      // Append the option element as a child of the select tag
      selectTag.innerHTML += option

    }
}
}
catch (e) {
    console.error('error', e)
  }

  const formTag = document.getElementById('create-location-form')
  formTag.addEventListener('submit', async event => {
    event.preventDefault()
    const formData = new FormData(formTag)
    const json = JSON.stringify(Object.fromEntries(formData))
  

    const locationUrl = 'http://localhost:8000/api/locations/'
    const fetchConfig = {
    method: "post",
    body: json,
    headers: {
        'Content-Type': 'application/json',
    },
    }
    const response = await fetch(locationUrl, fetchConfig)
    if (response.ok) {
    formTag.reset()
    const newLocation = await response.json()
  }  
  })
  
}
)
