function createCard(name, description, pictureUrl,location,dates) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${dates}
        </div>
      </div>
    `
  }


window.addEventListener('DOMContentLoaded', async() => {
    
  const url = 'http://localhost:8000/api/conferences/'

  try {
    const response = await fetch(url)

    if (!response.ok) {
        throw new Error('Response not ok')
    } else {
        const data = await response.json()

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`
          const detailResponse = await fetch(detailUrl)
          if (detailResponse.ok) {
            const details = await detailResponse.json()
            const title = details.conference.name
            const description = details.conference.description
            const pictureUrl = details.conference.location.picture_url
            const startdate = new Date(Date.parse(details.conference.starts))
            const enddate = new Date(Date.parse(details.conference.ends))
            const start = startdate.getMonth() + "/" + startdate.getDate() + "/" + startdate.getFullYear()
            const end = enddate.getMonth() + "/" + enddate.getDate() + "/" + enddate.getFullYear()
            const dates = start +" - " + end
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, location, dates)
            const cardholder = document.querySelector('.cardholder');
            cardholder.innerHTML += html;
          }
        }
        const cont = document.querySelector('.cardholder')
        cont.style.display = 'flex'
        cont.style.flexFlow = "column wrap"
        cont.style.gap = '10px 10px'
        const cards = document.querySelectorAll('.card')
        for (let card of cards){
            card.style.width = '31%'
            card.style.boxShadow = "7px 7px 12px"
        }
        cont.style.height = '1600px'
        
  
      }
    
  } catch (e) {
    console.error('error', e)
  }

})
