window.addEventListener('DOMContentLoaded', async() => {

    const locations_url = "http://localhost:8000/api/locations"
    
    try{
    const locResponse = await fetch(locations_url) 
    
    if (!locResponse.ok){
        throw new Error('Response not ok')
    }
    else{
        const locData = await locResponse.json()
    
        const selectTag = document.getElementById('location')
        for (let location of locData.locations) {
          // Create an 'option' element
            const option = `<option value="${location.id}">${location.name}</option>`
          // Append the option element as a child of the select tag
          selectTag.innerHTML += option
    
        }
    }
    }
    catch (e) {
        console.error('error', e)
      }
    
      const formTag = document.getElementById('create-conference-form')
      formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
    
        const conferenceURL = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        }
        const response = await fetch(conferenceURL, fetchConfig)
        if (response.ok) {
        formTag.reset()
        const newConference = await response.json()
      }  
      })
      
    }
    )